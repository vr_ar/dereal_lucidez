using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioCoroutines : MonoBehaviour
{
    [SerializeField] private AudioSource SFXMistAid, SFXSuelta;
    [SerializeField] private AudioSource SFXAlarm, SFXHola, SFXNoNece, SFXSal;
    [SerializeField] private int secondsTilPlay;
    [SerializeField] private GameObject flower;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(mistAid());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator mistAid()
    {
        yield return new WaitForSeconds(secondsTilPlay);
        SFXMistAid.Play();

    }

    public void StartFlowerCR()
    {
        StartCoroutine("flowerDisappear");
    }

    public void StartBarandaSFXCR()
    {
        StartCoroutine("barandaSFXCR");
    }

    private IEnumerator flowerDisappear()
    {
        yield return new WaitForSeconds(3);
        flower.SetActive(false);
    }

    private IEnumerator barandaSFXCR()
    {
        yield return new WaitForSeconds(5);
        SFXSuelta.Play();
    }

    public void StartEndingSFX()
    {
        StartCoroutine(WaitForReadingLetter());
    }

    private IEnumerator WaitForReadingLetter()
    {
        yield return new WaitForSeconds(6);

        SFXAlarm.loop = false;
        SFXAlarm.Stop();

        SFXHola.Play();

        yield return new WaitForSeconds(6);

        SFXNoNece.Play();
        SFXSal.Play();

        //Aqu� ir�a otro yield return con los audios de las voces de ellas
    }
}
