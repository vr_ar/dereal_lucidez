using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoleActivator : MonoBehaviour
{
    public GameObject leftHand_indicator;
    public GameObject rightHand_indicator;

    public GameObject arbolPuzzle;
    public GameObject poleGrab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void CartaGrabbed()
    {
        poleGrab.SetActive(true);
        arbolPuzzle.transform.DOScale(0, 5).OnComplete(() =>
        {
            leftHand_indicator.SetActive(true);
            rightHand_indicator.SetActive(true);
            
        });
    }
}
