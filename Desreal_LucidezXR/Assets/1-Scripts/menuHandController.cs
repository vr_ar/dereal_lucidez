using System.Collections;
using UnityEngine;
using DG.Tweening;

public class menuHandController : MonoBehaviour
{
    [SerializeField] private GameObject handGO;
    public float moveDuration = 1f;
    public float moveDistance = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(HandAppear());
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator HandAppear()
    {
        yield return new WaitForSeconds(15);
        handGO.SetActive(true);
        StartDiagonalMovement();
    }

    private void StartDiagonalMovement()
    {
        // Calcula la posici�n de destino en diagonal hacia la izquierda y hacia arriba
        Vector3 targetPosition = new Vector3(handGO.transform.position.x - moveDistance, handGO.transform.position.y + moveDistance, handGO.transform.position.z);

        // Movimiento en loop de forma diagonal con DOTween
        handGO.transform.DOMove(targetPosition, moveDuration)
                        .SetLoops(-1, LoopType.Yoyo)
                        .SetEase(Ease.Linear);
    }
}
