using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private OVRPassthroughLayer passthroughLayer;
    [SerializeField] private GameObject LetrasRojasBtn;
    [SerializeField] private List<GameObject> Letters;
    [SerializeField] private AudioSource SFXScribbles, SFXScribblesL, SFXScribblesR, SFXTrain;


    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject l in Letters)
        {
            l.transform.localScale = Vector3.zero;
        }
        
        StartCoroutine(FadeLetters());
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator FadeLetters()
    {
        yield return new WaitForSeconds(5f);
        SFXScribbles.Play();
        SFXScribblesL.Play();
        SFXScribblesR.Play();
        SFXTrain.Play();
        FadeInLetters();
    }
    public void FadeInLetters()
    {
        for (int i = 0; i < Letters.Count; i++) { Letters[i].transform.DOScale(1,2).OnComplete(()=>
        {
            DOTween.Kill(Letters[i]);
        }); }
    }
    public void SpawnWorldObjects(bool logic)
    {
        StartCoroutine(SpawnWorldObjects_Corutine(logic));
    }

    private IEnumerator SpawnWorldObjects_Corutine(bool logic)
    {
        LetrasRojasBtn.transform.DOScale(Vector3.zero, 1.2f);
        yield return new WaitForSeconds(2.2f);
        passthroughLayer.gameObject.SetActive(false);
        SceneManager.LoadScene(1);
        
    }
}
