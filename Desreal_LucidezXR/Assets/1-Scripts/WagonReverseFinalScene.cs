using DG.Tweening;
using Oculus.Interaction.Samples;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class WagonReverseFinalScene : MonoBehaviour
{
    public Transform ambieteExterior;
    private Vector3 newPosition = new Vector3(1.638f, -0.496f, -0.265f);
    private bool isMoving = false;
    [SerializeField] private List<ParticleSystem> leavesParticles;


    bool leftGrab = false;
    bool rightGrab = false;

    public UnityEvent OnLettingPoleGo;

    void Update()
    {
        
    }

    public void HandGrabbed(int index)
    {  
        switch (index)
        {
            case 0:

                Debug.LogError("You must indicate a number between 1 and 4 to decide what to do");
                break;

            case 1:

                rightGrab = true;
                //Debug.LogWarning("Is Right Hand Grabbed?: " + rightGrab);
                break;

            case 2:

                leftGrab = true;
                //Debug.LogWarning("Is Left Hand Grabbed?: " + leftGrab);
                break;

            case 3:
                rightGrab = false;
                Debug.LogWarning("Is Right Hand Grabbed?: " + rightGrab);

                if (!leftGrab && !rightGrab)
                {
                    Debug.LogWarning("Comming Back...");

                    StartCoroutine(ComeBackToReality());
                }
                break;

            case 4:
                leftGrab = false;
                Debug.LogWarning("Is Left Hand Grabbed?: " + leftGrab);

                if (!leftGrab && !rightGrab)
                {
                    Debug.LogWarning("Comming Back...");

                    StartCoroutine(ComeBackToReality());
                }
                break;
        }

        if (leftGrab && rightGrab)
        {
            Debug.LogWarning("Is holding the pole with both hands");

            StartReverse();
        }
        
    }

    private void StartReverse()
    {
        for (int i = 0; i < leavesParticles.Count; i++)
        {
            leavesParticles[i].Stop();
        }

        ambieteExterior.position = newPosition;
        ambieteExterior.DOMoveX(33, 5).SetEase(Ease.InCubic).OnComplete(() =>
        {
            DOTween.Kill(ambieteExterior);
            LoopReverse();
        });
    }

    private void LoopReverse()
    {
        if (!isMoving)
        {
            isMoving = true;
            ambieteExterior.position = newPosition;
            ambieteExterior.DOMoveX(34.8f, 2).SetEase(Ease.Linear).SetLoops(-1);
        }
    }

    public void EndReverse()
    {
        DOTween.KillAll(ambieteExterior);
    }

    private IEnumerator ComeBackToReality()
    {
        OnLettingPoleGo.Invoke();

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(0);
        DOTween.KillAll(ambieteExterior);
    }

}
