using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GrowModelController : MonoBehaviour
{
    [Range(0,1)]
    public float minGrow = 0f;
    [Range(0, 1)]
    public float maxGrow = 1f;

    public float transitionDurationStems = 6f;
    public float transitionDurationFlowers = 3f;

    public GameObject arbolPuzzle;

    [SerializeField] private List<Material> flowerMaterials = new List<Material>();
    [SerializeField] private List<Material> stemMaterials = new List<Material>();

    [SerializeField] private AudioSource SFXGrow, SFXMorse;

   void Start()
    {
        arbolPuzzle.transform.localScale = Vector3.zero;

        foreach (var stems in stemMaterials)
        {
            stems.SetFloat("_Grow", minGrow);
        }

        foreach (var flowers in flowerMaterials)
        {
            flowers.SetFloat("_Grow", minGrow);
        }
    }

   /* void Update()
    {
        if (Input.GetKeyDown(KeyCode.I)) 
        {
            for(int i = 0; i < growModelMaterials.Count; i++) 
            {
                StartCoroutine(GrowModels(growModelMaterials[i]));
            }
        }
    }*/

   /*IEnumerator GrowModels (Material mat) 
    {
        float growValue = mat.GetFloat("_Grow");

        if (!fullyGrown) 
        {
            while(growValue < maxGrow) 
            {
                growValue += 1 / (timeToGrow / refreshRate);
                mat.SetFloat("_Grow", growValue);

                yield return new WaitForSeconds(refreshRate);
            }
        }
        else 
        {
            while(growValue > minGrow) 
            {
                growValue -= 1 / (timeToGrow / refreshRate);
                mat.SetFloat("_Grow", growValue);

                yield return new WaitForSeconds(refreshRate);
            }
        }

        if(growValue >= maxGrow) 
        {
            fullyGrown = true;
        }
        else 
        {
            fullyGrown= false;
        }
    }*/

    public void GrowingModels()
    {
        
        foreach (Material mat in stemMaterials)
        {
           mat.DOFloat(maxGrow, "_Grow", transitionDurationStems).OnComplete(() =>
           {
               foreach (Material mat2 in flowerMaterials)
               {
                   mat2.DOFloat(maxGrow, "_Grow", transitionDurationFlowers).OnComplete(() =>
                   {
                       DOTween.Kill(flowerMaterials);
                       DOTween.Kill(stemMaterials);

                       arbolPuzzle.transform.DOScale(1, transitionDurationStems*2).OnComplete(() =>
                       {

                           DOTween.Kill(arbolPuzzle);
                       });
                   });
               }
           });

        }

        SFXGrow.Play();

        StartCoroutine("waitForMorseSFX");
    }

    private IEnumerator waitForMorseSFX()
    {
        yield return new WaitForSeconds(4);
        SFXMorse.Play();
    }
}
