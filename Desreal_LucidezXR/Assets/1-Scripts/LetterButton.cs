using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterButton : MonoBehaviour
{
    Material m_Material;
    private void Start()
    {
        m_Material = GetComponent<Renderer>().material;
    }
    public void HoverLetter()
    {
        m_Material.color = Color.red;
    }
    public void UnHoverLetter()
    {
        m_Material.color = Color.white;
    }
    public void SelectLetter()
    {
        m_Material.color = Color.green;
    }
    public void UnSelectLetter()
    {
        m_Material.color = Color.white;
    }
}
