using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class TreeButtonsPuzzle : MonoBehaviour
{

    [SerializeField] private int pressedButtons = 0;

    [SerializeField] private bool isComplete = false;
    [SerializeField] private bool hasStarted = false;
    public float timeToComplete;
    [SerializeField] private float currentTime;
    [SerializeField] private AudioSource SFXWrong, SFXAlarm, SFXMorse, SFXHola, SFXNoNece, SFXSal;

    [Header("Buttons List")]
    [SerializeField] List<GameObject> ButtonVisual = new List<GameObject>();
    [SerializeField] List<GameObject> ButtonTriggered = new List<GameObject>();

    [Tooltip("Place any actions that you want to execute when the puzzle is completed")]
    public UnityEvent onPuzzleCompleted;

    void Start()
    {
        currentTime = 0;
        pressedButtons = 0;
    }

    void Update()
    {
        if (hasStarted)
        {
            currentTime += Time.fixedDeltaTime;
        }

        if (hasStarted && currentTime >= timeToComplete)
        {
            ResetPuzzle();
        }

        if (pressedButtons >= ButtonVisual.Count && !isComplete)
        {
            isComplete = true;
            onPuzzleCompleted.Invoke();
            Debug.LogWarning("HTE PUZZLE IS COMPLETE");
            SFXAlarm.loop = true;
            SFXAlarm.Play();
            SFXMorse.Stop();
        }
    }

    private void ResetPuzzle()
    {
        Debug.LogWarning("The puzzle has been restarted");

        SFXWrong.Play();

        hasStarted = false;
        isComplete = false;
        currentTime = 0;
        pressedButtons = 0;

        for (int i = 0; i < ButtonTriggered.Count; i++)
        {
            ButtonVisual[i].SetActive(true);
            ButtonTriggered[i].SetActive(false);
        }
    }

    public void ButtonPressed(int index)
    {
        hasStarted = true;

        ButtonVisual[index-1].gameObject.SetActive(false);
        ButtonTriggered[index-1].gameObject.SetActive(true);

        pressedButtons++;
    }

    //private IEnumerator WaitForReadingLetter()
    //{
    //    yield return new WaitForSeconds(6);
    //    SFXAlarm.loop = false;
    //    SFXAlarm.Stop();

    //    SFXHola.Play();

    //    yield return new WaitForSeconds(6);

    //    SFXNoNece.Play();
    //    SFXSal.Play();

    //    //Aqu� ir�a otro yield return con los audios de las voces de ellas
    //}
}
