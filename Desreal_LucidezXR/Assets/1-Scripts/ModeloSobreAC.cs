using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeloSobreAC : MonoBehaviour
{
    [SerializeField] Animator mAnimator;
    [SerializeField] GameObject letter;
    [SerializeField] PopUp PopUp;

    public void OpenSobre()
    {
        mAnimator.SetTrigger("TrOpen");
        StartCoroutine(LetterAppear());
    }

    IEnumerator LetterAppear()
    {
        yield return new WaitForSeconds(1.2f);
        letter.SetActive(true);
        PopUp.FadeInPopUps();
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
