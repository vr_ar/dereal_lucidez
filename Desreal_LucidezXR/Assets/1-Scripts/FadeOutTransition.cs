using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FadeOutTransition : MonoBehaviour
{
    [SerializeField] private Image FadeOutImage;
    public float transitionTime = 2.0f;

    void Start()
    {
        FadeOutImage.color = Color.black;
        FadeIn();
    }

    void Update()
    {
        
    }

    public void FadeInFadeOut()
    {
        FadeOutImage.DOColor(Color.black, transitionTime).OnComplete(() => {
            FadeOutImage.DOColor(Color.clear, transitionTime).OnComplete(() =>
            {
                DOTween.Kill(FadeOutImage);
            });
        });
    }

    public void FadeIn()
    {
        FadeOutImage.DOColor(Color.clear, transitionTime).OnComplete(() => { 
            DOTween.Kill(FadeOutImage);
        });
    }

    public void FadeOut()
    {
        FadeOutImage.DOColor(Color.black, transitionTime).OnComplete(() => {
            DOTween.Kill(FadeOutImage);
        });
    }
}
