using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerGrab_Event : MonoBehaviour
{
    public GrowModelController growModelController;
    [SerializeField] private AudioSource SFXGrabFlower, SFXSparkling;
  
    public void OnFlowerGrab_Event()
    {
        SFXSparkling.Stop();
        SFXGrabFlower.Play();
        growModelController.GrowingModels();
    }
}
