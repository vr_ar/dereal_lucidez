using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveSmoke : MonoBehaviour
{
    [SerializeField] private Material smoke1;
    public float transitionDuration = 2f;
    [SerializeField] private GameObject Flower;
    [SerializeField] private AudioSource SFXMist, SFXSparkling;

    [SerializeField] private List<ParticleSystem> leavesParticles;

    // Start is called before the first frame update
    void Start()
    {
        smoke1.SetFloat("_FogDissolve", 4);
        smoke1.SetFloat("_AlphaFloat", 1);
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void MoveSmokeScreen()
    {
        SFXMist.Play();

        for (int i = 0; i < leavesParticles.Count; i++)
        {
            leavesParticles[i].Play();
        }

        smoke1.DOFloat(300, "_FogDissolve", transitionDuration).OnComplete(() =>
        {
            smoke1.DOFloat(0, "_AlphaFloat", transitionDuration/2).OnComplete(() =>
            {
                
                DOTween.Kill(smoke1);
                Flower.SetActive(true);


                SFXSparkling.loop = true;
                SFXSparkling.Play();

                gameObject.SetActive(false);
            });
        });
        
    }
}
