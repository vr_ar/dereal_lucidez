using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PopUp : MonoBehaviour
{   
    [SerializeField] private List<GameObject> popups;
    [SerializeField] private GameObject sobre;
    [SerializeField] private AudioSource SFXPop;
    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject l in popups)
        {
            l.transform.localScale = Vector3.zero;
        }
    }
    IEnumerator SobreCor()
    {
        yield return new WaitForSeconds(1f);
        sobre.SetActive(true);
        yield return new WaitForSeconds(1.2f);
        sobre.GetComponent<ModeloSobreAC>().OpenSobre();

    }
    public void FadeOutPopUps()
    {
        popups[0].transform.DOScale(0f, 0.5f);
        popups[1].transform.DOScale(0f, 0.8f);
    }
    public void FadeInPopUps()
    {
        SFXPop.Play();

        popups[0].transform.DOScale(0.85f, 1.2f).OnComplete(() => { FadeOutPopUps(); });
        popups[1].transform.DOScale(0.1f,0.8f);

    }
    public void Sobre()
    {
        StartCoroutine(SobreCor());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
